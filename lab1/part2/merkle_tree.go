package main

import (
	"bytes"
	"crypto/sha256"
	"fmt"
)

// MerkleTree represents a merkle tree
type MerkleTree struct {
	RootNode *Node
	Leafs    []*Node
}

// Node represents a merkle tree node
type Node struct {
	Parent *Node
	Left   *Node
	Right  *Node
	Hash   []byte
}

const (
	leftNode = iota
	rightNode
)

// MerkleProof represents way to prove element inclusion on the merkle tree
type MerkleProof struct {
	proof [][]byte
	index []int64
}

// NewMerkleTree creates a new Merkle tree from a sequence of data
func NewMerkleTree(data [][]byte) *MerkleTree {
	var nodes []Node
	tree := MerkleTree{nil, nil}

	for _, d := range data {
		node := NewMerkleNode(nil, nil, d)
		nodes = append(nodes, *node)
		tree.Leafs = append(tree.Leafs, node)
	}

	if len(nodes) == 0 {
		return nil
	}
	for len(nodes) > 0 {
		if len(nodes) % 2 != 0 {
			lastTx := nodes[len(nodes) - 1]
			nodes = append(nodes, lastTx)
		}

		var height []Node
		for i := 0; i < len(nodes)/2; i++ {
			node := NewMerkleNode(&nodes[i], &nodes[i+1], nil)
			height = append(height, *node)
		}
		nodes = height
	}
	tree.RootNode = &nodes[0]
	return &tree
}

// NewMerkleNode creates a new Merkle tree node
func NewMerkleNode(left, right *Node, data []byte) *Node {
	node := Node{}

	node.Left = left
	node.Right = right

	if left == nil && right == nil { //Leaf Node
		hash := sha256.Sum256(data)
		node.Hash = hash[:]
	} else {
		childHashes := append(node.Left.Hash, node.Right.Hash...)
		hash := sha256.Sum256(childHashes)
		node.Hash = hash[:]
		// Set Parents
		node.Left.Parent = &node
		node.Right.Parent = &node
	}
	node.Left = left
	node.Right = right
	node.Parent = nil

	return &node
}

// MerkleRootHash return the hash of the merkle root
func (mt *MerkleTree) MerkleRootHash() []byte {
	return mt.RootNode.Hash
}

// MakeMerkleProof returns a list of hashes and indexes required to
// reconstruct the merkle path of a given hash
//
// @param hash represents the hashed data (e.g. transaction ID) stored on
// the leaf node
// @return the merkle proof (list of intermediate hashes), a list of indexes
// indicating the node location in relation with its parent (using the
// constants: leftNode or rightNode), and a possible error.
func (mt *MerkleTree) MakeMerkleProof(hash []byte) ([][]byte, []int64, error) {
	check := false
	var node Node
	for _, leaf := range mt.Leafs {
		if bytes.Compare(leaf.Hash, hash) == 0 {
			check = true
			node = *leaf
			break
		}
	}
	if !check {
		return [][]byte{}, []int64{}, fmt.Errorf("Node %x not found", hash)
	}
	var path [][]byte
	var indices []int64
	for bytes.Compare(node.Hash, mt.RootNode.Hash) != 0 {
		parent := node.Parent
		if bytes.Compare(node.Hash, parent.Left.Hash) != 0 {
			path = append(path, parent.Left.Hash)
			indices = append(indices, leftNode)
		} else {
			path = append(path, parent.Right.Hash)
			indices = append(indices, rightNode)
		}
		node = *parent
	}

	return path, indices, nil
}

// VerifyProof verifies that the correct root hash can be retrieved by
// recreating the merkle path for the given hash and merkle proof.
//
// @param rootHash is the hash of the current root of the merkle tree
// @param hash represents the hash of the data (e.g. transaction ID)
// to be verified
// @param mProof is the merkle proof that contains the list of intermediate
// hashes and their location on the tree required to reconstruct
// the merkle path.
func VerifyProof(rootHash []byte, hash []byte, mProof MerkleProof) bool {
	var parentHash [32]byte
	// Assuming The path starts with leaf node at last index
	for i := len(mProof.index) - 1; i >= 0; i++ {
		nodeHash := mProof.proof[i]
		if mProof.index[i] == leftNode {
			parentHash = sha256.Sum256(append(nodeHash, hash...))
		} else {
			parentHash = sha256.Sum256(append(hash, nodeHash...))
		}
		hash = parentHash[:]
		if bytes.Compare(rootHash, hash) == 0 {
			break
		}
	}
	if bytes.Compare(rootHash, hash) == 0 {
		return true
	}
	return false
}
