package main

import (
	"bytes"
	"fmt"
	"time"
)

// https://en.bitcoin.it/wiki/File:Jonny1000thetimes.png
const genesisCoinbaseData = "The Times 03/Jan/2009 Chancellor on brink of second bailout for banks"

// Blockchain keeps a sequence of Blocks
type Blockchain struct {
	blocks []*Block
}

// CreateBlockchain creates a new blockchain with genesis Block
func CreateBlockchain() *Blockchain {
	coinBaseTx := Transaction{[]byte(genesisCoinbaseData) }
	genesisBlock := NewGenesisBlock(&coinBaseTx)
	genesisBlock.Timestamp = time.Now().Unix()
	genesisBlock.PrevBlockHash = nil
	genesisBlock.SetHash()
	return &Blockchain{
		[]*Block{genesisBlock},
	}
}

// NewBlockchain creates a Blockchain
func NewBlockchain() *Blockchain {
	return CreateBlockchain()
}

// AddBlock saves the block into the blockchain
func (bc *Blockchain) AddBlock(transactions []*Transaction) *Block {
	lastBlock := bc.CurrentBlock()
	newBlock := NewBlock(transactions, lastBlock.Hash)
	bc.blocks = append(bc.blocks, newBlock)
	return newBlock
}

// GetGenesisBlock returns the Genesis Block
func (bc *Blockchain) GetGenesisBlock() *Block {
	return bc.blocks[0]
}

// CurrentBlock returns the last block
func (bc *Blockchain) CurrentBlock() *Block {
	return bc.blocks[len(bc.blocks) - 1]
}

// GetBlock returns the block of a given hash
func (bc *Blockchain) GetBlock(hash []byte) (*Block, error) {
	for _, b := range bc.blocks {
		if bytes.Compare(b.Hash, hash) == 0 {
			return b, nil
		}
	}
	return nil, fmt.Errorf("block not found")
}
