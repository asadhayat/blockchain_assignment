package main

import (
	"bytes"
	"crypto/sha256"
	"strconv"
	"time"
)

// Block keeps block information
type Block struct {
	Timestamp     int64          // the block creation timestamp
	Transactions  []*Transaction // The block transactions
	PrevBlockHash []byte         // the hash of the previous block
	Hash          []byte         // the hash of the block
}

// NewBlock creates and returns Block
func NewBlock(transactions []*Transaction, prevBlockHash []byte) *Block {
	block := &Block{time.Now().Unix(), transactions, prevBlockHash, []byte{}}
	block.SetHash()
	return block
}

// NewGenesisBlock creates and returns genesis Block
func NewGenesisBlock(coinbase *Transaction) *Block {
	return NewBlock([]*Transaction{coinbase}, []byte{})
}

// SetHash calculates and sets the block hash
func (b *Block) SetHash() {
	var headers []byte
	headers = bytes.Join([][]byte {
		[]byte(strconv.FormatInt(b.Timestamp, 10)),
		b.HashTransactions(),
		b.PrevBlockHash,
	}, []byte{})

	blockHash := sha256.Sum256(headers)
	b.Hash = blockHash[:]
}

// HashTransactions returns a hash of the transactions in the block
func (b *Block) HashTransactions() []byte {
	var merkleTree MerkleTree
	txHashes := [][]byte{}
	// You should compute the hash of all transactions
	for _, tx := range b.Transactions {
		hash :=  sha256.Sum256(tx.Data)
		txHashes = append(txHashes, hash[:])
	}
	merkleTree = *(NewMerkleTree(txHashes))

	return merkleTree.RootNode.Hash[:]
}
